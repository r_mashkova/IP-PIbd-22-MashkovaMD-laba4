package ru.ulstu.is.sbapp.student.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.student.service.StudentService;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return new StudentDto(studentService.findStudent(id));
    }

    @GetMapping("/")
    public List<StudentDto> getStudents() {
        return studentService.findAllStudents().stream()
                .map(StudentDto::new)
                .toList();
    }

    @PostMapping("/")
    public StudentDto createStudent(@RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName) {
        return new StudentDto(studentService.addStudent(firstName, lastName));
    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Long id,
                                    @RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName) {
        return new StudentDto(studentService.updateStudent(id, firstName, lastName));
    }

    @DeleteMapping("/{id}")
    public StudentDto deleteStudent(@PathVariable Long id) {
        return new StudentDto(studentService.deleteStudent(id));
    }
}