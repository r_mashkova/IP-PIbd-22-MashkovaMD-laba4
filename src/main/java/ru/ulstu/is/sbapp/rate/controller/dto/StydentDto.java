package ru.ulstu.is.sbapp.rate.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulstu.is.sbapp.rate.model.Stydent;
import ru.ulstu.is.sbapp.rate.model.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StydentDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    private String firstName;
    private String lastName;
    private Boolean hostelStatus;
    private GroupeDto groupe;
    private List<Long> subjects;

    public StydentDto(){
    }

    public StydentDto(Stydent student) {
        this.id = student.getId();
        this.firstName = student.getFirstName();
        this.lastName = student.getLastName();
        this.hostelStatus = student.getHostelStatus();
        if(student.getGroupe() != null){
            this.groupe = new GroupeDto(student.getGroupe());
        }
        this.subjects = new ArrayList<>();
        List<Subject> subjectList = student.getSubjects();
        if(subjectList != null) {
            for (Subject subject : subjectList) {
                this.subjects.add(subject.getId());
            }
        }
        else {
            this.subjects = Collections.emptyList();
        }
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getHostelStatus() {
        return hostelStatus;
    }

    public GroupeDto getGroupe(){
        return groupe;
    }

    public List<Long> getSubjects(){
        return subjects;
    }
}