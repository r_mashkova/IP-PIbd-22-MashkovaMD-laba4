package ru.ulstu.is.sbapp.rate.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulstu.is.sbapp.rate.model.Groupe;

public class GroupeDto {
    private Long id;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String name;

    public GroupeDto() {
    }

    public GroupeDto(Groupe groupe) {
        this.id = groupe.getId();
        this.name = String.format("%s", groupe.getGroupName());
    }

    public long getId() {
        return id;
    }
    public String getName() { return name; }
}
