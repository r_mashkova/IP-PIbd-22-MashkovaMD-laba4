package ru.ulstu.is.sbapp.rate.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulstu.is.sbapp.rate.model.Stydent;
import ru.ulstu.is.sbapp.rate.model.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubjectDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    private String name;
    private List<Long> students;

    public SubjectDto(){

    }

    public SubjectDto(Subject subject) {
        this.id = subject.getId();
        this.name = String.format("%s", subject.getSubjectName());
        this.students = new ArrayList<>();
        List<Stydent> stydentList = subject.getStudents();
        if(stydentList != null) {
            for (Stydent stydent : stydentList) {
                this.students.add(stydent.getId());
            }
        }
        else {
            this.students = Collections.emptyList();
        }
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Long> getStudents() {
        return students;
    }
}
