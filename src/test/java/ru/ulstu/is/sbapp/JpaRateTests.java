package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.rate.model.Groupe;
import ru.ulstu.is.sbapp.rate.model.Stydent;
import ru.ulstu.is.sbapp.rate.model.Subject;
import ru.ulstu.is.sbapp.rate.service.*;
import ru.ulstu.is.sbapp.rate.service.exception.GroupeNotFoundException;
import ru.ulstu.is.sbapp.rate.service.exception.StydentNotFoundException;
import ru.ulstu.is.sbapp.rate.service.exception.SubjectNotFoundException;

import java.util.List;

@SpringBootTest
public class JpaRateTests {
    private static final Logger log = LoggerFactory.getLogger(JpaStudentTests.class);
    //TODO: переделать
    @Autowired
    private StydentService studentService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private GroupeService groupeService;
    /*
    //StudentTests
    @Test
    void testStudentCreate() {
        studentService.deleteAllStudents();
        final Stydent student = studentService.addStudent("Иван", "Иванов", true);
        log.info(student.toString());
        Assertions.assertNotNull(student.getId());
    }

    @Test
    void testStudentRead() {
        studentService.deleteAllStudents();
        final Stydent student = studentService.addStudent("Иван", "Иванов", true);
        log.info(student.toString());
        final Stydent findStudent = studentService.findStudent(student.getId());
        log.info(findStudent.toString());
        Assertions.assertEquals(student, findStudent);
    }

    @Test
    void testStudentReadNotFound() {
        studentService.deleteAllStudents();
        Assertions.assertThrows(StydentNotFoundException.class, () -> studentService.findStudent(-1L));
    }

    @Test
    void testAddingSubjectToStudent(){
        subjectService.deleteAllSubjects();
        studentService.deleteAllStudents();
        Subject subject = subjectService.addSubject("ИП");
        Stydent student = studentService.addStudent("Иван", "Иванов", true);
        subjectService.addSubjectStudent(subject.getId(), student.getId());
        student = studentService.findStudent(student.getId());
        subject = subjectService.findSubject(subject.getId());
        Assertions.assertEquals(student.getSubjects().get(0), subject);
    }

    @Test
    void testStudentReadAll() {
        studentService.deleteAllStudents();
        studentService.addStudent("Иван", "Иванов", true);
        studentService.addStudent("Петр", "Петров", false);
        final List<Stydent> students = studentService.findAllStudents();
        log.info(students.toString());
        Assertions.assertEquals(students.size(), 2);
    }

    @Test
    void testStudentReadAllEmpty() {
        studentService.deleteAllStudents();
        final List<Stydent> students = studentService.findAllStudents();
        log.info(students.toString());
        Assertions.assertEquals(students.size(), 0);
    }
    */
    //GroupTests
    /*@Test
    void testGroupCreate() {
        groupeService.deleteAllGroups();
        final Groupe groupe = groupeService.addGroup("ПИбд-22");
        log.info(groupe.toString());
        Assertions.assertNotNull(groupe.getId());
    }*/
    /*
    @Test
    void testGroupRead() {
        groupeService.deleteAllGroups();
        final Groupe groupe = groupeService.addGroup("ПИбд-22");
        log.info(groupe.toString());
        final Groupe findGroup = groupeService.findGroup(groupe.getId());
        log.info(findGroup.toString());
        Assertions.assertEquals(groupe, findGroup);
    }*/

    @Test
    void testGroupReadNotFound() {
        groupeService.deleteAllGroups();
        Assertions.assertThrows(GroupeNotFoundException.class, () -> groupeService.findGroup(-1L));
    }
/*
    @Test
    void testGroupReadAll() {
        groupeService.deleteAllGroups();
        groupeService.addGroup("ПИбд-22");
        groupeService.addGroup("ПИбд-21");
        final List<Groupe> groups = groupeService.findAllGroups();
        log.info(groups.toString());
        Assertions.assertEquals(groups.size(), 2);
    }*/
    /*
    @Test
    void testGroupReadAllEmpty() {
        groupeService.deleteAllGroups();
        final List<Groupe> groups = groupeService.findAllGroups();
        log.info(groups.toString());
        Assertions.assertEquals(groups.size(), 0);
    }

    //SubjectTests
    @Test
    void testSubjectCreate() {
        subjectService.deleteAllSubjects();
        final Subject subject = subjectService.addSubject("Интернет программирование");
        log.info(subject.toString());
        Assertions.assertNotNull(subject.getId());
    }

    @Test
    void testSubjectRead() {
        subjectService.deleteAllSubjects();
        final Subject subject = subjectService.addSubject("Интернет программирование");
        log.info(subject.toString());
        final Subject findSubject = subjectService.findSubject(subject.getId());
        log.info(findSubject.toString());
        Assertions.assertEquals(subject, findSubject);
    }

    @Test
    void testSubjectReadNotFound() {
        subjectService.deleteAllSubjects();
        Assertions.assertThrows(SubjectNotFoundException.class, () -> subjectService.findSubject(-1L));
    }

    @Test
    void testSubjectReadAll() {
        subjectService.deleteAllSubjects();
        subjectService.addSubject("Интернет программирование");
        subjectService.addSubject("Технологии программирования");
        final List<Subject> subjects = subjectService.findAllSubjects();
        log.info(subjects.toString());
        Assertions.assertEquals(subjects.size(), 2);
    }

    @Test
    void testSubjectReadAllEmpty() {
        subjectService.deleteAllSubjects();
        final List<Subject> subjects = subjectService.findAllSubjects();
        log.info(subjects.toString());
        Assertions.assertEquals(subjects.size(), 0);
    }*/
}
